﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CardFile.Domain.Entities;
using CardFile.UserService.Infrastructure;
using CardFile.UserService.Models;
using Microsoft.AspNetCore.Identity;

namespace CardFile.UserService
{
    public class UserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<ApplicationUserModel> AddAsync(ApplicationUserModel model)
        {
            if (model is null)
            {
                throw new OperationDetails("Argument is wrong", nameof(model));
            }

            var applicationUser = new ApplicationUser()
            {
                UserName = model.UserName,
                Email = model.Email,
                FullName = model.FullName
            };

            try
            {
                var result = await _userManager.CreateAsync(applicationUser, model.Password); ;
                if (result.Errors.Any())
                    throw new OperationDetails(result.Errors.FirstOrDefault()?.Description);
                return model;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
