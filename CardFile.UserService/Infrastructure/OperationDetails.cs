﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;

namespace CardFile.UserService.Infrastructure
{
    [Serializable]
    public class OperationDetails: Exception
    {
        public OperationDetails(string message, string prop): base(message)
        {
            Property = prop;
        }
        public string Property { get; private set; }
        public OperationDetails(string message) : base(message)
        {
        }

        public OperationDetails(string message, Exception innerException) : base(message, innerException)
        {

        }

        protected OperationDetails(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
