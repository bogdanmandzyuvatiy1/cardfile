﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardFile.Application.Models
{
    public class AuthorModel
    {
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public ICollection<CardModel> Cards { get; set; }

    }
}
