﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardFile.Application.Models
{
    public class CardModel
    {
        public int CardId { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public string Publisher { get; set; }
        public DateTime PublicationDate { get; set; }
        public int PagesNumber { get; set; }
        public ICollection<AuthorModel> Authors{ get; set; }
        public GenreModel Genre { get; set; }
        public FileModel File{ get; set; }

    }
}
