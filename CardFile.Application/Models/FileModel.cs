﻿using System.IO;

namespace CardFile.Application.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CardModel Card { get; set; }
        public string Path { get; set; }
    }
}
