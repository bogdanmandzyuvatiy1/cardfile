﻿using System;
using System.Runtime.Serialization;
using System.Text.Json;

namespace CardFile.Application.Validation
{
    [Serializable]
    public class CardFileException: Exception
    {
        public string Property { get; protected set; }
        public int Status { get; set; } = 400;

        public CardFileException(string message, string prop) : base(message)
        {
            Property = prop;
        }

        public CardFileException(string message): base(message)
        {
        }

        public CardFileException(string message, Exception innerException): base(message, innerException)
        {
           
        }

        protected CardFileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
