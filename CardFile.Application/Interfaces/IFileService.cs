﻿using System.Threading.Tasks;
using CardFile.Application.Models;
using CardFile.Domain.Entities;

namespace CardFile.Application.Interfaces
{
    public interface IFileService: ICrud<FileModel>
    {
    }
}
