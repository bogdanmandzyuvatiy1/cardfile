using System.Linq;
using AutoMapper;
using CardFile.Application.Models;
using CardFile.Domain.Entities;

namespace CardFile.Application
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Card, CardModel>()
                .ForMember(d => d.Authors, o => o.MapFrom(s => s.CardAuthors.Select(ca => ca.Author)))
                .ForMember(d => d.Genre, o => o.MapFrom(s => s.Genre))
                .ForMember(d => d.File, o => o.MapFrom(f => f.File))
                .ReverseMap();

            CreateMap<Author, AuthorModel>()
                .ForMember(d => d.Cards, o => o.MapFrom(s => s.CardAuthors.Select(b=>b.Card)))
                .ReverseMap();

            CreateMap<File, FileModel>()
                .ForMember(x => x.Card, o => o.MapFrom(x => x.Card))
                //.ForMember(d => d.Address, opt => opt.MapFrom(s => s.ReaderProfile.Address))
                //.ForMember(d => d.CardsIds, opt => opt.MapFrom(s => s.Cards.Select(c => c.Id)))
                //.ForMember(d => d.Id, opt => opt.MapFrom(s => s.ReaderProfile.ReaderId))
                .ReverseMap();

            CreateMap<Genre, GenreModel>()
                .ForMember(d => d.Cards, o => o.MapFrom(s => s.Cards))
                .ReverseMap();

        }
    }
}