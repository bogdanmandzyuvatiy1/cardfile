﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.Validation;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;

namespace CardFile.Application.Services
{
    public class GenreService : IGenreService
    {
        private readonly IMapper _mapper;

        public GenreService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork _unitOfWork { get; }

        public async Task<int> AddAsync(GenreModel model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var entity = _mapper.Map<Genre>(model);
            await _unitOfWork.GenreRepository.AddAsync(entity);
            await _unitOfWork.SaveAsync();
            return entity.GenreId;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.GenreRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<GenreModel>> GetAllAsync()
        {
            var source = await _unitOfWork.CardRepository
                .FindAllWithDetailsAsync();

            return _mapper.Map<IEnumerable<GenreModel>>(source);
        }

        public async Task<GenreModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.CardRepository
                .GetByIdWithDetailsAsync(id);

            return _mapper.Map<GenreModel>(source);
        }

        public async Task UpdateAsync(GenreModel model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var genre = await _unitOfWork.GenreRepository.GetByIdWithDetailsAsync(model.GenreId);
            _mapper.Map(model, genre);
            await _unitOfWork.SaveAsync();
        }
    }
}
