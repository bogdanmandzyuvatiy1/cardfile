﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.Validation;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;

namespace CardFile.Application.Services
{
    public class FileService : IFileService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public FileService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> AddAsync(FileModel model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var entity = _mapper.Map<File>(model);
            await _unitOfWork.FileRepository.AddAsync(entity);
            await _unitOfWork.SaveAsync();
            return entity.Id;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.FileRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<FileModel>> GetAllAsync()
        {
            var source = await _unitOfWork.FileRepository
                .FindAllWithDetailsAsync();

            return _mapper.Map<IEnumerable<FileModel>>(source);
        }

        public async Task<FileModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.FileRepository
                .GetByIdWithDetailsAsync(id);

            return _mapper.Map<FileModel>(source);
        }

        public async Task UpdateAsync(FileModel model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var file = await _unitOfWork.FileRepository.GetByIdWithDetailsAsync(model.Id);
            _mapper.Map(model, file);
            await _unitOfWork.SaveAsync();
        }
    }
}
