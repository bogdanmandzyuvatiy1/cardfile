﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.Validation;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;

namespace CardFile.Application.Services
{
    public class CardService : ICardService
    {
        private readonly IMapper _mapper;

        public CardService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork _unitOfWork { get; }

        public async Task<int> AddAsync(CardModel  model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var entity = _mapper.Map<Card>(model);
            await _unitOfWork.CardRepository.AddAsync(entity);
            await _unitOfWork.SaveAsync();
            return entity.CardId;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.CardRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<CardModel>> GetAllAsync()
        {
            var source = await _unitOfWork.CardRepository
                .FindAllWithDetailsAsync();

            return _mapper.Map<IEnumerable<CardModel>>(source);
        }

        public async Task<CardModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.CardRepository
                .GetByIdWithDetailsAsync(id);

            return _mapper.Map<CardModel>(source);
        }

        public async Task UpdateAsync(CardModel model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var card = await _unitOfWork.CardRepository.GetByIdWithDetailsAsync(model.CardId);
            _mapper.Map(model, card);
            await _unitOfWork.SaveAsync();
        }
    }
}
