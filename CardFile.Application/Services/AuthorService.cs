﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.Validation;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;

namespace CardFile.Application.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IMapper _mapper;

        public AuthorService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork _unitOfWork { get; }

        public async Task<int> AddAsync(AuthorModel model)
        {
            if (model is null )
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var entity = _mapper.Map<Author>(model);
            await _unitOfWork.AuthorRepository.AddAsync(entity);
            await _unitOfWork.SaveAsync();
            return entity.AuthorId;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.AuthorRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<AuthorModel>> GetAllAsync()
        {
            var source = await _unitOfWork.AuthorRepository
                .FindAllWithDetailsAsync();

            return _mapper.Map<IEnumerable<AuthorModel>>(source);
        }

        public async Task<AuthorModel> GetByIdAsync(int id)
        {
            var source = await _unitOfWork.AuthorRepository
                .GetByIdWithDetailsAsync(id);

            return _mapper.Map<AuthorModel>(source);
        }

        public async Task UpdateAsync(AuthorModel model)
        {
            if (model is null)
            {
                throw new CardFileException("Argument is wrong", nameof(model));
            }

            var author = await _unitOfWork.AuthorRepository.GetByIdWithDetailsAsync(model.AuthorId);
             _mapper.Map(model, author);
            await _unitOfWork.SaveAsync();
        }
    }
}
