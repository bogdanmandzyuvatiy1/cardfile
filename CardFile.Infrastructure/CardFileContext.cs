﻿using CardFile.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CardFile.Infrastructure
{
    public class CardFileContext: DbContext
    {
        public DbSet<File> Files { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Genre> Genres { get; set; }

        public CardFileContext(DbContextOptions<CardFileContext> options)
            : base(options)
        {
        }
    }
}
