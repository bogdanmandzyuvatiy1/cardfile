﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;

namespace CardFile.Infrastructure.Interfaces
{
    public interface IGenreRepository : IRepository<Genre>
    {
        Task<IEnumerable<Genre>> FindAllWithDetailsAsync();
        Task<Genre> GetByIdWithDetailsAsync(int id);
    }
}
