﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;

namespace CardFile.Infrastructure.Interfaces
{
    public interface IAuthorRepository : IRepository<Author>
    {
        Task<IEnumerable<Author>> FindAllWithDetailsAsync();

        Task<Author> GetByIdWithDetailsAsync(int id);
    }
}
