﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;

namespace CardFile.Infrastructure.Interfaces
{
    public interface ICardRepository: IRepository<Card>
    {
        Task<IEnumerable<Card>> FindAllWithAuthorsAsync();

        Task<Card> GetByIdWithAuthorsAsync(int id);

        Task<IEnumerable<Card>> FindAllWithDetailsAsync();


        Task<Card> GetByIdWithDetailsAsync(int id);
    }
}

