﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Repositories;

namespace CardFile.Infrastructure.Interfaces
{
    public interface IFileRepository: IRepository<File>
    {
        Task<IEnumerable<File>> FindAllWithDetailsAsync();
        Task<File> GetByIdWithDetailsAsync(int id);
    }
}
