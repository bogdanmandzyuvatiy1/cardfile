﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Infrastructure.Interfaces
{
    public interface IUnitOfWork
    {
        IFileRepository FileRepository { get; }

        IAuthorRepository AuthorRepository { get; }
        IGenreRepository GenreRepository { get; }
        ICardRepository CardRepository { get; }
        Task<int> SaveAsync();
    }
}
