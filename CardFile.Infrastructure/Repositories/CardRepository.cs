﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CardFile.Infrastructure.Repositories
{
    class CardRepository: Repository<Card>, ICardRepository
    {
        public CardRepository(CardFileContext cardFileContext): base(cardFileContext)
        {
            
        }

        public async Task<IEnumerable<Card>> FindAllWithAuthorsAsync()
        {
            return await _dbSet.Include(c => c.CardAuthors).ThenInclude(ca => ca.Card).ToListAsync();
        }

        public async Task<Card> GetByIdWithAuthorsAsync(int id)
        {
            return await _dbSet.Include(c => c.CardAuthors).ThenInclude(ca => ca.Card).SingleOrDefaultAsync(x => x.CardId == id);
        }

        public async Task<IEnumerable<Card>> FindAllWithDetailsAsync()
        {
            return await _dbSet.Include(c => c.Genre).Include(c => c.File).ToListAsync();
        }

        public async Task<Card> GetByIdWithDetailsAsync(int id)
        {
            return await _dbSet.Include(c => c.Genre).Include(c => c.File).SingleOrDefaultAsync(x => x.CardId == id);
        }
    }
}
