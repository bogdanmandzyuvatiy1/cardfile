﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CardFile.Infrastructure.Repositories
{
    class GenreRepository: Repository<Genre>, IGenreRepository
    {
        public GenreRepository(CardFileContext cardFileContext): base(cardFileContext)
        {
            
        }

        public async Task<IEnumerable<Genre>> FindAllWithDetailsAsync()
        {
            return await _dbSet.Include(g => g.Cards).ToListAsync();
        }

        public async Task<Genre> GetByIdWithDetailsAsync(int id)
        {
            return await _dbSet.Include(g => g.Cards).SingleOrDefaultAsync(g => g.GenreId == id);
        }
    }
}
