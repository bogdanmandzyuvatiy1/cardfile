﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CardFile.Infrastructure.Repositories
{
    public class FileRepository: Repository<File>, IFileRepository
    {
        public FileRepository(CardFileContext cardFileContext): base(cardFileContext)
        {
        }

        public async Task<IEnumerable<File>> FindAllWithDetailsAsync()
        {
            return await _dbSet.Include(f => f.Card).ToListAsync();
        }

        public async Task<File> GetByIdWithDetailsAsync(int id)
        {
            return await _dbSet.Include(f => f.Card).SingleOrDefaultAsync(f => f.Id == id);
        }
    }
}
