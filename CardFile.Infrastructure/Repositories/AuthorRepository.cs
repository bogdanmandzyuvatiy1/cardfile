﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardFile.Domain.Entities;
using CardFile.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CardFile.Infrastructure.Repositories
{
    class AuthorRepository: Repository<Author>, IAuthorRepository
    {
        public AuthorRepository(CardFileContext cardFileContext): base(cardFileContext)
        {
            
        }

        public async Task<IEnumerable<Author>> FindAllWithDetailsAsync()
        {
            return await _dbSet.Include(a => a.CardAuthors).ThenInclude(ca => ca.Card).ToListAsync();
        }

        public async Task<Author> GetByIdWithDetailsAsync(int id)
        {
            return await _dbSet.Include(a => a.CardAuthors).ThenInclude(ca => ca.Card).SingleOrDefaultAsync(a => a.AuthorId == id);
        }
    }
}
