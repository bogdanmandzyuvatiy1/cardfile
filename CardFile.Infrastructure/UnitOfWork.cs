﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CardFile.Infrastructure.Interfaces;
using CardFile.Infrastructure.Repositories;

namespace CardFile.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(CardFileContext cardFileContext)
        {
            _context = cardFileContext;
        }

        private readonly CardFileContext _context;
        private IFileRepository _fileRepository;
        private IAuthorRepository _authorRepository;
        private IGenreRepository _genreRepository;
        private ICardRepository _cardRepository;

        public IFileRepository FileRepository => _fileRepository ??= new FileRepository(_context);
        public IAuthorRepository AuthorRepository => _authorRepository ??= new AuthorRepository(_context);
        public IGenreRepository GenreRepository => _genreRepository ??= new GenreRepository(_context);
        public ICardRepository CardRepository => _cardRepository ??= new CardRepository(_context);

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed && disposing)
            {

                _context.Dispose();
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

