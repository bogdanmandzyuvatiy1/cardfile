﻿
using CardFile.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CardFile.Infrastructure
{
    public class AuthenticationContext: IdentityDbContext<ApplicationUser>
    {
        public AuthenticationContext(DbContextOptions options): base(options)
        {
            
        }
    }
}
