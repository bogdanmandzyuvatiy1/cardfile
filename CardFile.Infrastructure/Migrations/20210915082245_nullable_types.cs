﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CardFile.Infrastructure.Migrations
{
    public partial class nullable_types : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Genres_GenreId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Files_Cards_CardId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_CardId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "CardId",
                table: "Files");

            migrationBuilder.AlterColumn<int>(
                name: "GenreId",
                table: "Cards",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "FileId",
                table: "Cards",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cards_FileId",
                table: "Cards",
                column: "FileId",
                unique: true,
                filter: "[FileId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Files_FileId",
                table: "Cards",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Genres_GenreId",
                table: "Cards",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "GenreId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Files_FileId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Genres_GenreId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_FileId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "FileId",
                table: "Cards");

            migrationBuilder.AddColumn<int>(
                name: "CardId",
                table: "Files",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "GenreId",
                table: "Cards",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Files_CardId",
                table: "Files",
                column: "CardId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Genres_GenreId",
                table: "Cards",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "GenreId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Cards_CardId",
                table: "Files",
                column: "CardId",
                principalTable: "Cards",
                principalColumn: "CardId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
