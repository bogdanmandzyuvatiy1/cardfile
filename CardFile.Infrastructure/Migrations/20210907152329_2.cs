﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CardFile.Infrastructure.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NumberPages",
                table: "Cards",
                newName: "PagesNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PagesNumber",
                table: "Cards",
                newName: "NumberPages");
        }
    }
}
