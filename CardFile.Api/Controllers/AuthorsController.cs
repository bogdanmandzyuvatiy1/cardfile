﻿using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardFile.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private IAuthorService AuthorService { get; }

        public AuthorsController(IAuthorService authorService)
        {
            AuthorService = authorService;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<AuthorModel>>> GetById(int id)
        {
            var result = await AuthorService.GetByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<AuthorModel>>> Get()
        {
            var result = await AuthorService.GetAllAsync();

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult> Add([FromBody] AuthorModel authorModel)
        {
            var authorId = await AuthorService.AddAsync(authorModel);

            return CreatedAtAction(nameof(GetById), new { Id = authorModel.AuthorId }, authorModel);
        }

        [HttpPut]
        public async Task<ActionResult> Update(AuthorModel authorModel)
        {
            await AuthorService.UpdateAsync(authorModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await AuthorService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
