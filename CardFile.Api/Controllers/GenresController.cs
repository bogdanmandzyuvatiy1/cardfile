﻿using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardFile.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private IGenreService GenreService { get; }

        public GenresController(IGenreService genreService)
        {
            GenreService = genreService;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<GenreModel>>> GetById(int id)
        {
            var result = await GenreService.GetByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult> Add([FromBody] GenreModel genreModel)
        {
            var authorId = await GenreService.AddAsync(genreModel);

            return CreatedAtAction(nameof(GetById), new { Id = authorId }, genreModel);
        }

        [HttpPut]
        public async Task<ActionResult> Update(GenreModel genreModel)
        {
            await GenreService.UpdateAsync(genreModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await GenreService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
