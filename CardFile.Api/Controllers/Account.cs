﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardFile.UserService.Models;
using CardFile.UserService;
using Microsoft.AspNetCore.Identity;

namespace CardFile.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Account : ControllerBase
    {
        private readonly UserService.UserService _userService;

        public Account(UserService.UserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> Post(ApplicationUserModel model)
        {
            return Ok(await _userService.AddAsync(model));
        }
    }
}
