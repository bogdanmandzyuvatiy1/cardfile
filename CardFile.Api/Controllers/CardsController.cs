﻿using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardFile.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardsController : ControllerBase
    {
        private ICardService CardService { get; }

        public CardsController(ICardService cardService)
        {
            CardService = cardService;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CardModel>>> GetById(int id)
        {
            var result = await CardService.GetByIdAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult> Add([FromBody] CardModel cardModel)
        {
            var authorId = await CardService.AddAsync(cardModel);

            return CreatedAtAction(nameof(GetById), new { Id = authorId }, cardModel);
        }

        [HttpPut]
        public async Task<ActionResult> Update(CardModel cardModel)
        {
            await CardService.UpdateAsync(cardModel);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await CardService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
