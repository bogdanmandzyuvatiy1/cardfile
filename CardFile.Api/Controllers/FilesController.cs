﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CardFile.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IFileService _fileService;
        private readonly IWebHostEnvironment _environment;

        public FilesController(IFileService fileService, IWebHostEnvironment environment)
        {
            _fileService = fileService;
            _environment = environment;
        }

        // GET: api/<FileController>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<FileController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}



        //// PUT api/<FileController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<FileController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}


        // POST api/<FileController>
        [HttpPost]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            if (files == null)
            {
                return NotFound();
            }
            var target = Path.Combine(_environment.ContentRootPath, "Files");

            foreach (var file in files)
            {
                if (file.Length <= 0)
                {
                    Debug.WriteLine("File length is 0");
                    continue;
                };
                var filePath = Path.Combine(target, file.FileName);
                await using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                FileModel fileModel = new FileModel { Name = file.FileName, Path = filePath };
                await _fileService.AddAsync(fileModel);
            };
            
            return Ok();
            
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Download(int id)
        {
            var fileModel = await _fileService.GetByIdAsync(id);

            var path = fileModel.Path;

            var memory = new MemoryStream();
            await using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                    {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }


}
