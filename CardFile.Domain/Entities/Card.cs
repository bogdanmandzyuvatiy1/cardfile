﻿using System;
using System.Collections.Generic;

namespace CardFile.Domain.Entities
{
    public class Card
    {
        public int CardId { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public string Publisher { get; set; }
        public DateTime PublicationDate { get; set; }
        public int PagesNumber { get; set; }
        public int? GenreId { get; set; }
        public int? FileId { get; set; }

        public ICollection<CardAuthor> CardAuthors { get; set; }
        public Genre Genre { get; set; }
        public File File { get; set; }
    }
}
