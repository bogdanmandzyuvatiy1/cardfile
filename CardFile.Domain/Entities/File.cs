﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardFile.Domain.Entities
{
    public class File
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public Card Card { get; set; }
    }
}
