﻿using System.Collections.Generic;

namespace CardFile.Domain.Entities
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Card> Cards { get; set; }
    }
}
