﻿namespace CardFile.Domain.Entities
{
    public class CardAuthor
    {
        public int Id { get; set; }
        public int CardId { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
        public Card Card { get; set; }
    }
}
