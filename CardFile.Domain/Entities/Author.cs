﻿using System.Collections.Generic;

namespace CardFile.Domain.Entities
{
    public class Author
    {
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public ICollection<CardAuthor> CardAuthors { get; set; }
    }
}
